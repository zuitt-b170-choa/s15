// Mathematical Operators

/*
	+ Sum
	- Difference
	* Product
	/ Quotient
	% Remainder

	used when the value of the first number is stored in a variable; performs the operation as well as storing the resulting value in that variable
	+= Addition
	-= Subtraction
	*= Multiplication
	/= Division
	%= Modulo (returns 0 if there are no remainders)
*/

function mod(){
	// return 9+2
	// return 9-2
	// return 9*2
	// return 9/2 = 4 r1
	// return 9%2
	let x = 10;
	// return x += 2
	// return x -= 2
	// return x *= 2
	// return x /= 2
	return x %= 2
};

console.log(mod());

// Assignment Operator (=) is used for assigning values to variables
let x = 1;
let sum = 1;
// sum = sum +1
x -= 1;
sum += 1;

 console.log(sum);
 console.log(x);

 // Increment & Decrement

// Increment(++)

// Pre-increment - adding 1 BEFORE assigning the value to the variable
 let z = 1;
 let increment = ++z;
 // let z = (1) + 1;

 console.log(`Result of pre-increment: ${increment}`);
 console.log(`Result of pre-increment: ${z}`);

// Post-increment -  adding 1 AFTER assigning the value to the variable
increment = z++;
// let z = 1 + (1)

 console.log(`Result of pre-increment: ${increment}`);
 console.log(`Result of pre-increment: ${z}`);

 // Decrement

 // pre-decrement - subtracting 1 BEFORE the assigning of values (basing from the most recent value of z)
let decrement = --z;
console.log(`Result of pre-decrement: ${decrement}`);
console.log(`Result of pre-decrement: ${z}`);

// Post-decrement - subtracting 1 AFTER assigning of values
decrement = z--
// z = 2 - (1)
console.log(`Result of post-decrement: ${decrement}`);
console.log(`Result of post-decrement: ${z}`);



// Comparison Operators


// Equality Operator (==) - compares if the two values are equal
let juan = "juan";

console.log( 1 == 1 );
console.log( 0 == false );
console.log(juan == "juan");


// Strict Equality ( === )
// use case = navigating in the webpages Course, Course Outline
/*if ( Data == Course ){
	course.html
}*/
/*if ( Data == Course Outline ){
	courseOutline.html
}*/
// the javascript is not anymore concerned with other relative functions of the data, it only compares the two values as they are;
console.log ( 1 === true );
console.log( juan === "Juan");

// inequality ( != )
console.log( 1 != 1 );
console.log( juan != "juan");

//strict inequality (!==)
// the javascript is not anymore concerned with other relative functions of the data, it only compares the two values as they are;
console.log( 0 !== false );

// Other Comparison Operators
/*
	> Greater Than
	< Less Than
	>= Greater than or equal
	<= Less than or equal
*/

//Logical Operators
/*
	AND Operator (&&) - returns true if all operands are true;
	true + true =  true
	true + false =  false
	false + true = false
	false + false = false

	OR Operator (||) - returns true if one of the operands are true;
	true + true = true
	true + false = true
	false + true = true
	false + false = false
*/

// And Operator
let isLegalAge = true;
let isRegistered = true;

let allRequirementMet = isLegalAge && isRegistered;
console.log(`Result of logical AND operator: ${allRequirementMet}`);

// Or Operator (||)
allRequirementMet = isLegalAge || isRegistered;
console.log(`Result of logical OR operator: ${allRequirementMet}`)

// Selection Control Structures

/*
IF Statements - executes a command if a specified condition is true
	SYNTAX:
		if (condition){
			statements/command
		}
*/
let num = -1;

if (num<0) {
	console.log("Hello")
};

let value = 30;
if (value > 10 && value < 40) {
	console.log(`Welcome to Zuitt`)
};

/*
if-else statement - executes a command if the first condition returns false
	SYNTAX
		if(condition){
			statement if true
		}
		else{
			statement if false
		};
*/

num = 5
if (num > 10) {
	console.log(`Number is greater than 10`)
}
else{
	console.log(`Number is less than 10`)
};

/*
	prompt - dialog box that has input fields
	alert - dialog box that has no input fields; used for warnings, announcements. etc.
	parseInt - converts number in string data type into numerical data type. it only recognizes numbers and ignores the alphabets (it returns NaN - Not a Number)
*/
/*num = parseInt(prompt("Pease input a number."));
if (num>59) {
	alert("Senior Age")
	console.log(num)
}
else{
	alert("Invalid Age")
	console.log(num)
};
*/
// if-elseif-else statement - multiple separate, yet connected conditions
	/*
		if (condition) {
			statement
		}
		else if (condition){
			statement
		}
		else if (condition){
			statement
		}
		else if (condition){
			statement
		}
		.
		.
		.
		else{
			statement
		}
	*/

/*
	1. Quezon
	2. Valenzuela
	3. Pasig
	4. Taguig
*/
/*let city = parseInt(prompt("Enter a Number"));
if (city === 1) {
	alert("Welcome to Quezon City")
}
else if (city === 2){
	alert("Welcome to Valenzuela City")
}
else if ( city === 3 ){
	alert("Welcome to Pasig City")
}
else if ( city === 4 ){
	alert("Welcome to Taguig City")
}
else{
	alert("Invalid Number")
};*/

let message = "";

function determineTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return	`Not a typhoon yet.`
	}
	else if (windspeed <= 61) {
		return `Tropical Depression detected.`
	}
	else if (windspeed >=62 && windspeed <= 88){
		return `Tropical Storm detected.`
	}
	else if (windspeed >= 89 && windspeed <= 117) {
		return	`Severe Tropical Storm detected`
	}
	else{
		return `Typhoon detected`
	}
};
message = determineTyphoonIntensity(120);
console.log(message);

/*
	Ternary operator - shorthanded if-else statement
		SYNTAX:
			(condition) ? ifTrue : ifFalse
*/
let ternaryResult = (1 < 18) ? "1" : "0";
console.log(`Result of ternary operator: ${ternaryResult}`);

let name;
function isOfLegalAge(){
	name = "John";
	return "You are of the legal age Limit"
};

function isUnderAge(){
	name = "Jane";
	return "You are under the legal Age"
};
/*let age = parseInt(prompt("What is Your Age?"));
let LegalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
if (age >= 18) {
	isOfLegalAge()
}else{
	isUnderAge()
}
alert( `Result of Ternary Operator in Function: ${LegalAge} ${name}` );*/

// Switch Statement - shorthand for if-elseif-else should there be a lot of conditions to be met.
	// break - signals the browser that when the case value has been met, no need to go further down the command
/*
	SYNTAX
		switch (expression/parameter){
			case value1:
				statement/s;
				break;
			case value2:
				statement/s;
				break;
			case value3:
				statement/s;
				break;
			case value4:
				statement/s;
				break;
			.
			.
			.
			default:
				statements;
		}
*/

/*let day = prompt("What day is it today?").toLowerCase();
switch (day){
	case "sunday":
		alert("The color of the day is red");
		break;
	case "monday":
		alert("The color of the day is orange");
		break;
	case "tuesday":
		alert("The color of the day is yellow");
		break;
	case "wednesday":
		alert("The color of the day is green");
		break;
	case "thursday":
		alert("The color of the day is blue");
		break;
	case "friday":
		alert("The color of the day is violet");
		break;
	case "saturday":
		alert("The color of the day is indigo");
		break;
	default:
		alert("Please input a valid day");
};*/

// Try-Catch-Finally Statement
/*
	commonly used for error handling
*/

function showIntentsityAlert(windspeed){
	try{
		alert(determineTyphoonIntensity(windspeed));
	}
	catch(error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert")
	}
}
showIntentsityAlert(56);
// showIntentsityAlert(1e); (WITH ERROR)
