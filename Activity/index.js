console.log(`Hello World`);

let input1 = parseInt(prompt("Provide a number: "));
let input2 = parseInt(prompt("Provide another number: "));
let totalSum;

function sumInput(){
	totalSum = input1 + input2;
	return totalSum;
}

sumInput();

if (totalSum < 10){
	let total = input1 + input2;
	console.warn(`The sum of the two numbers is ${total}.`);

}
else if (totalSum >= 10 && totalSum <= 20){
	let total = input1 - input2;
	alert(`The difference of the two numbers is ${total}.`);
}
else if (totalSum > 20 && totalSum <= 29){
	let total = input1 * input2;
	alert(`The product of the two numbers is ${total}.`);
	
}
else if (totalSum >= 30){
	let total = input1 / input2;
	alert(`The quotient of the two numbers is ${total}.`);
}
else{
	alert(`Enter a valid number.`);
}

let name = prompt("What is your name?");
let age = parseInt(prompt("What is your age?"));

if(name == null || age == null){
	alert(`Are you a time traveler?`);
}
else if(name !== null && age !== null){
	alert(`Hello ${name}. Your age is ${age}.`);
}

function isLegalAge(){
	alert(`You are of legal age.`);
}
function isUnderAge(){
	alert(`You are not allowed here.`);
}

if(age < 18){
	isUnderAge();
}
else if(age >= 18){
	isLegalAge();
}

switch (age){
	case 18:
		alert("You are now allowed to party.");
		break;
	case 21:
		alert("You are now part of the adult society.");
		break;
	case 65:
		alert("We thank you for your contribution to society.");
		break;
	default:
		alert("Are you sure you're not an alien?");
}

function ageAlert(age){
	try{
		alert(isLegalAg(age));
	}
	catch(error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert("Are you really not an alien?");
	}
}
ageAlert(12);